function Question(question, answers, correct){
	this.question = question;
	this.answers = answers;
	this.correct = correct;
}

Question.prototype.displayQuestion = function(){
	document.querySelector('.qst').textContent = this.question;

	var elementNumb = document.querySelectorAll('.ans');

	if(elementNumb){
		for (var i = 0; i < elementNumb.length; i++) {
			var parent = document.getElementById("qst-ans");
			var child = document.querySelector('.ans');
			parent.removeChild(child);
		}
	}

	for (var i = 0; i < this.answers.length; i++){
		var tag = document.createElement("p");
		tag.className = "ans";
		var ans = document.createTextNode(i + ': ' + this.answers[i]);
		tag.appendChild(ans);
		var parent = document.getElementById("qst-ans");
		parent.appendChild(tag);
	}
}

Question.prototype.checkAnswer = function(ans, callback){
	var sc;
	var result = document.querySelector('.corr');
	
	if(ans === this.correct){
		var classRed = document.querySelector('.red');

		if(classRed){
			document.querySelector('.corr').classList.remove('red');
		}

		result.style.visibility="visible";
		result.textContent = "Correct!";
		result.className += " green";
		sc = callback(true);
	}else{
		var classGreen = document.querySelector('.green');

		if(classGreen){
			document.querySelector('.corr').classList.remove('green');
		}

		result.style.visibility="visible";
		result.textContent = "Nope! Try again.";
		result.className += " red";

		sc = callback(false);
	}

	this.displayScore(sc);
}

Question.prototype.displayScore = function(score){
	document.querySelector('.sco').textContent = score;
}

var q1 = new Question('Is JavaScript the coolest programming language in the world?', ['Yes', 'No'], 0);
var q2 = new Question('What is the name of this course\'s teacher?', ['Michael', 'John', 'Jonas'], 2);
var q3 = new Question('What does best describe coding?', ['Boring', 'Hard', 'Fun', 'Tendence'], 2);

var questions = [q1, q2, q3];

function score(){
	var sc = 0;
	return function(correct){
		if(correct){
			sc++;
		}
		return sc;
	}
}

var keepScore = score();

function nextQuestion(){
	var n = Math.floor(Math.random() * questions.length);
	questions[n].displayQuestion();

	document.querySelector('.ans-val').value = "";
	document.querySelector('.corr').style.visibility="hidden";

	var submitFunction = function(){
		var answer;
		answer = document.querySelector('.ans-val').value;
		questions[n].checkAnswer(parseInt(answer), keepScore);
	};

	document.querySelector('.btn-sub').onclick = submitFunction;
}

window.addEventListener('load', nextQuestion);
document.querySelector('.btn-next').addEventListener('click',nextQuestion);
