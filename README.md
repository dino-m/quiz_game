#Quiz game

JS simple application where you need to answer the correct question and get some scores, JS file describe simple use of objects.

###DESCRIPTION:

- JS pick some random question that is created like an object.
- Application display's question and possible answers.
- The user needs to type in the correct number that is in front of every possible answer.
- There's a function in JS that check if answer is correct, if it is, it display's "Correct" string and increases score for 1, otherwise it just display's string "Nope! Try again.".
- If user want to continue he just need to click NEXT QUESTION.
- If user want to end the game, he need to click THAT'S ENOUGH FOR ME.